package com.example.myble;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import java.util.UUID;

import static android.bluetooth.BluetoothProfile.STATE_CONNECTED;
import static android.bluetooth.BluetoothProfile.STATE_DISCONNECTED;


public class MainActivity extends AppCompatActivity {

    private BluetoothManager    bluetoothManager;
    private BluetoothAdapter    bluetoothAdapter;
    private BluetoothLeScanner  bluetoothLesScanner;
    private BluetoothGatt       bluetoothGatt;
    final static UUID SERVICE_UUID = UUID.fromString("6e400001-b5a3-f393-e0a9-e50e24dcca9e");
    final static UUID CHARACTERISTIC_UUID = UUID.fromString("6e400003-b5a3-f393-e0a9-e50e24dcca9e");
    final static UUID CLIENT_CHARACTERISTIC_CONFIG_UUID = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");

    private BluetoothGattCallback myGattCallback = new BluetoothGattCallback(){
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            if (newState == STATE_CONNECTED) {

                Log.i("asdf", "Connected to GATT server.");
                Log.i("asdf", "Attempting to start service discovery:" +
                        bluetoothGatt.discoverServices());

            } else if (newState == BluetoothProfile.STATE_DISCONNECTED) {

                Log.i("asdf", "Disconnected from GATT server.");
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            super.onServicesDiscovered(gatt, status);
            if(status == BluetoothGatt.GATT_SUCCESS){
                Log.d("asdf","IN SUCCESS");
                BluetoothGattService service = gatt.getService(SERVICE_UUID);
                if(service != null){
                    Log.d("asdf","SERVICE");
                    BluetoothGattCharacteristic characteristic = service.getCharacteristic(CHARACTERISTIC_UUID);
                    if(characteristic != null){
                        Log.d("asdf","CHARACTERISTIC");
                        int properties = characteristic.getProperties();
                        if((properties|BluetoothGattCharacteristic.PROPERTY_NOTIFY)>0){
                            gatt.setCharacteristicNotification(characteristic,true);
                        }
                        BluetoothGattDescriptor descriptor = characteristic.getDescriptor(CLIENT_CHARACTERISTIC_CONFIG_UUID);
                        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
                        gatt.writeDescriptor(descriptor);
                        Log.d("asdf","test");
                    }
                }

            }
            else{
                Log.d("asdf","FAILED");
            }
        }

        @Override
        public void onCharacteristicChanged(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic) {
            super.onCharacteristicChanged(gatt, characteristic);
            if(characteristic.getUuid().equals(CHARACTERISTIC_UUID)){
                final byte[] data = characteristic.getValue();
                if(data != null && data.length > 0){
                    final StringBuilder stringBuilder = new StringBuilder(data.length);
                    for(byte byteChar : data)
                        stringBuilder.append(String.format("%02X ",byteChar));
                    Log.d("asdf",stringBuilder.toString());
                }
            }

        }
    };

    private ScanCallback myScanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            super.onScanResult(callbackType, result);
            String name = result.getDevice().getName();
            Log.d("asdf",result + "");
            if("HUSTAR_11".equals(name)){
                bluetoothGatt = result.getDevice().connectGatt(getApplicationContext(), false, myGattCallback);
                bluetoothLesScanner.stopScan(myScanCallback);
            }

        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.d("asdf","Scan Failed");
        }
    };




    @Override
    protected void onStart() {
        super.onStart();
        String[] permission_list = {
                Manifest.permission.ACCESS_COARSE_LOCATION
        };

        for(String permission : permission_list){
            if(checkCallingOrSelfPermission(permission) == PackageManager.PERMISSION_DENIED){
                requestPermissions(permission_list,123);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if(requestCode==123)
        {
            for(int i=0; i<grantResults.length; i++)
            {
                if(grantResults[i]==PackageManager.PERMISSION_GRANTED){
                    Log.d("asdf", "check permissions");
                }
                else {
                    Toast.makeText(getApplicationContext(),"Need permission", Toast.LENGTH_LONG).show();
                    finish();
                }
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //ActivityCompat.requestPermissions(this,new String[]{Manifest.permission.BLUETOOTH},1);
        bluetoothManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        bluetoothLesScanner = bluetoothAdapter.getBluetoothLeScanner();



        if(bluetoothAdapter == null || !bluetoothAdapter.isEnabled()){
            //블루투스를 지원하지 않거나 켜져있지 않으면 장치를끈다.
            Toast.makeText(this, "블루투스를 켜주세요", Toast.LENGTH_SHORT).show();
            finish();
        }
        bluetoothLesScanner.startScan(myScanCallback);

    }
}
